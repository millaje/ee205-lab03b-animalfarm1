///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 Feb 2021 
///////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "animals.h"

const char* animalGender(enum Gender gender){
   switch(gender){
      case(MALE):
         return "Male";
         break;
      case(FEMALE):
         return "Female";
         break;
   }
   return NULL;
};
/// Decode the enum Color into strings for printf()
// char* colorName (enum Color color) {
const char* colorName(enum Color color) {

   // @todo Map the enum Color to a string
   switch(color){
      case (BLACK):
         return "Black";
         break;
      case (WHITE):
         return "White";
         break;
      case (RED):
         return "Red";
         break;
      case (BLUE):
         return "Blue";
         break;
      case (GREEN):
         return "Green";
         break;
      case (PINK):
         return "Pink";
         break;
   }
   return NULL; // We should never get here
};


const char* fixed( bool isFixed){
   switch(isFixed){
      case 1 || true:
         return "Yes";
         break;
      case 0 || false:
         return "No";
         break;
   }
   return NULL;
};

