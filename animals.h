///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.h
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   2 Feb 2021 
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <stdio.h>


#pragma once

/// Define the maximum number of cats or dogs in our array-database
#define MAX_SPECIES (20)

/// Gender is appropriate for all animals in this database
// enum Gender // @todo fill this out from here...
enum Gender{MALE, FEMALE};
const char* animalGender(enum Gender gender);
/// Return a string for the name of the color
/// @todo this is for you to implement
enum Color{BLACK, WHITE, RED, BLUE, GREEN, PINK};
const char* colorName(enum Color color);

const char* fixed( bool isFixed);
