###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Jeraldine Milla <millaje@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   2 Feb 2021
###############################################################################

CC		= gcc
CFLAGS = -g -Wall

all: animalfarm

animalfarm:
	$(info You need to write your own Makefile)
	$(info I know you can do it)
	$(info for now type gcc -o animalfarm *.c)

main.o: main.c cat.c cat.h animals.c animals.h
	$(CC) $(CFLAGS) -c main.c

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c

animalfarm: animals.o cat.o main.o
	$(CC) $(CFLAGS) -o animalfarm animals.o cat.o main.o

clean:
	rm -f *.o animalfarm





